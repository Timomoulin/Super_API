let tableBody = document.querySelector("#tableAPI");
let dataList=document.querySelector("#listAPI");
let inputRecherche=document.querySelector("#recherche");

async function fetchLesAPI(url) {
    let reponse = await fetch(url);
    let data = await reponse.json();
    // console.log(data);
    return data.entries;
}

function affiche(desAPI) {
    let chaineHTML = "";
    let chaineDatalist="";
    for (let i = 0; i < desAPI.length; i++)
    {
        let uneAPI = desAPI[i];
    chaineHTML += "<tr>";
    chaineHTML += `<td>${i+1}</td>`;
    chaineHTML += `<td>${uneAPI.API}</td>`;
    chaineHTML += `<td>${uneAPI.Description}</td>`;
    chaineHTML += `<td><a href="${uneAPI.Link}">Lien</a></td>`;
    chaineHTML += `<td>${uneAPI.Category}</td>`;
    chaineHTML += "</tr>";
    chaineDatalist+=`<option value="${uneAPI.API}"/>`
    }
    dataList.innerHTML=chaineDatalist;
    tableBody.innerHTML=chaineHTML;
}

fetchLesAPI("https://api.publicapis.org/entries").then(function (resultatFetch) {
    affiche(resultatFetch)
});

inputRecherche.addEventListener("keyup",async function(){
    let nomRechercher=inputRecherche.value ;
    if(!!nomRechercher)
    {
    let desAPI=await fetchLesAPI("https://api.publicapis.org/entries?title="+nomRechercher);
    affiche(desAPI);
    }
    else{
        affiche(await fetchLesAPI("https://api.publicapis.org/entries"));
    }
})

inputRecherche.addEventListener("change",async function(){
    let nomRechercher=inputRecherche.value ;
    if(!!nomRechercher)
    {
    let desAPI=await fetchLesAPI("https://api.publicapis.org/entries?title="+nomRechercher);
    affiche(desAPI);
    }
    else{
        affiche(await fetchLesAPI("https://api.publicapis.org/entries"));
    }
})
